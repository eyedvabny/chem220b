##Chem 220B, Statistical Mechanics, Spring 2013


###MC: Monte Carlo simulation of hard shells
* Requires the [GNU Scientific Library](http://www.gnu.org/software/gsl/)

###MD: Lennard-Jones Verlet molecular dynamics
* Requires [Boost](http://www.boost.org) Random library
