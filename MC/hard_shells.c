/* Monte Carlo of Hard Spheres
 * Author: Eugene Yedvabny
 */

//Configuration parameters
#define density 0.06
#define spheres 216
#define sweeps 10500
#define equi_sweeps 500

#define do_probe 0
#define probes 8
#define probe_file_name "hs_probe"

#define do_track 0
#define tracker spheres/2
#define trajectory_file_name "hs_traj"

#define do_field 0
#define field_avg_file_name "hs_field_avg"
#define field_hist_file_name "hs_field_hist"

#define do_radial 1
#define num_radial_bins 300
#define max_radial_bin 10.0
#define radial_hist_file_name "hs_radial_hist_0.06"

//Necessary libs
#include <stdio.h>
#include <gsl/gsl_rng.h>
#include <math.h>
#include <time.h>

//Bookkeeping
#define diam 1.0

int main() {
    
    //Init the variables
    clock_t begin, end;
    double time_spent,sideLen,shift,displacement,probeSpace,
            X[spheres],Y[spheres],Z[spheres],
            trajX[sweeps-equi_sweeps],trajY[sweeps-equi_sweeps],trajZ[sweeps-equi_sweeps],
            probeX[probes],probeY[probes],probeZ[probes],
            trialX,trialY,trialZ,dX,dY,dZ,dR,
            Rk,wv,vol_factor,Rk_avg[100][2],Rk_hist[sweeps-equi_sweeps][3], //TODO: Figure out a pointer method
            radial_hist_step;
    int numPerSide,numProbesSide,count,percent,sweep,
        step,sphere,check,clash,probe,N2d,N3d,N4d,
        hist2d[spheres]={0},hist3d[spheres]={0},hist4d[spheres]={0},
        num_vectors,Rk_hist_factor,k,
        gr_hist[num_radial_bins]={0},gr_bin;
    
    //Start the countdown
    begin = clock();
    
    //Read out the parameters
    printf("Starting an MC run of %d spheres at density %4.2f for %d sweeps\n",spheres,density,sweeps);
    printf("First %d sweeps will be used for equilibration\n",equi_sweeps);
    
    if(do_probe == 1){
        printf("The space will be probed with %d evenly distributes probes of diameter 2,3,and 4\n",probes);
        printf("The histogram will be written to %s\n",probe_file_name);
    }
    
    if(do_track == 1){
        printf("Trajectory of particle %d will be written to %s\n",tracker,trajectory_file_name);
    }
    
    if(do_field == 1){
        printf("The density field averages will be written to %s\n",field_avg_file_name);
        printf("Density field running stats will be written to %s\n",field_hist_file_name);
    }
    
    if(do_radial == 1){
        printf("The radial distribution histogram will be written to %s\n",radial_hist_file_name);
    }
    
    //Initialize the random number generator
    //Use time() as a seed to avoid same runs
    gsl_rng* ranr = gsl_rng_alloc(gsl_rng_default);
    gsl_rng_set(ranr,time(NULL));
    
    //Calculate the box dimension L based off density
    numPerSide = cbrt(spheres);
    sideLen = numPerSide*diam/cbrt(density);
    shift = sideLen/numPerSide; //Initial spacing between sphere centers
    
    //Populate the initial lattice of spheres
    for (sphere=0; sphere<spheres; sphere++){
        X[sphere]=(sphere % numPerSide) * shift;
        Y[sphere]=((sphere / numPerSide) % numPerSide) * shift;
        Z[sphere]=(sphere / numPerSide / numPerSide) * shift;
    }
    
    //Only probing is enabled
    //Arrange the probes on the lattice
    if(do_probe == 1){
        //Calculate the probe positions
        numProbesSide = cbrt(probes);
        probeSpace = sideLen/numProbesSide;
    
        //Populate the lattice of probes
        for (probe=0; probe<probes; probe++){
            probeX[probe]=(probe % numProbesSide) * probeSpace;
            probeY[probe]=((probe / numProbesSide) % numProbesSide) * probeSpace;
            probeZ[probe]=(probe / numProbesSide / numProbesSide) * probeSpace;
        }
    }
    
    //Only if doing density field stats
    //Get the number of wavevectors we will be working with
    if(do_field == 1){
        num_vectors = round(5*sideLen/diam);
        vol_factor = sqrt(pow(sideLen,3));
        Rk_hist_factor = round(sideLen/2/diam);
    }
    
    //Only if calculating the radial density distribution
    //Get the bin width of the histogram
    if(do_radial == 1){
        radial_hist_step = max_radial_bin / num_radial_bins;
    }
    
    //Calculate the max displacement step
    //Since random is [-0.5,0.5) our displacement
    //is at max +- inter-sphere distance
    displacement = 2.0*(shift-diam);
    
    //Some bookeeping for percent indicator
    percent = sweeps/100;
    
    //Start the MC sweeping
    for(sweep=0; sweep<sweeps; sweep++){
        for(step=0; step<spheres; step++){
            //Pick a sphere to move
            sphere = gsl_rng_uniform_int(ranr,spheres);
            
            //Get the new coordinates
            trialX = X[sphere] + displacement*(gsl_rng_uniform(ranr)-0.5);
            trialY = Y[sphere] + displacement*(gsl_rng_uniform(ranr)-0.5);
            trialZ = Z[sphere] + displacement*(gsl_rng_uniform(ranr)-0.5);
            
            //Verify we are not clashing with any of the other spheres
            clash = 0;
            for(check = 0; check<spheres; check++){
                if(check != sphere){
                    dX = trialX - X[check];
                    dX -= sideLen*round(dX/sideLen);
                    
                    dY = trialY - Y[check];
                    dY -= sideLen*round(dY/sideLen);
                    
                    dZ = trialZ - Z[check];
                    dZ -= sideLen*round(dZ/sideLen);
                    
                    dR = sqrt(dX*dX + dY*dY + dZ*dZ);
                    
                    //Clash means spacing between two nodes > diameter
                    if (dR < diam){
                        clash = 1;
                        break; //No need to continue looping
                    }
                }
            }
            
            //If we didn't clash then commit the move
            if(clash == 0){
                X[sphere] = trialX;
                Y[sphere] = trialY;
                Z[sphere] = trialZ;
            }
        }
    
        //Do the measurements after equilibration
        if(sweep >= equi_sweeps){
            
            //Track the particle trajectory
            if(do_track == 1){
                //Commit the sweep to the trajectory
                trajX[sweep-equi_sweeps]=X[tracker];
                trajY[sweep-equi_sweeps]=Y[tracker];
                trajZ[sweep-equi_sweeps]=Z[tracker];
            }
            
            //Count how many spheres are in the probes
            if(do_probe == 1){
                for (probe=0; probe<probes; probe++){
                    N2d=0,N3d=0,N4d=0;
                    for(sphere=0;sphere<spheres;sphere++){

                        dX = X[sphere]-probeX[probe];
                        dX -= sideLen*round(dX/sideLen);

                        dY = Y[sphere]-probeY[probe];
                        dY -= sideLen*round(dY/sideLen);

                        dZ = Z[sphere]-probeZ[probe];
                        dZ -= sideLen*round(dZ/sideLen);

                        dR = sqrt(dX*dX+dY*dY+dZ*dZ);

                        if(dR < diam){
                            N2d++;
                        }
                        if(dR < 1.5*diam){
                            N3d++;
                        }
                        if(dR < 2*diam){
                            N4d++;
                        }
                    }
                    hist2d[N2d]++;
                    hist3d[N3d]++;
                    hist4d[N4d]++;
                }
            }
            
            //Calculate the microscopic density field
            if(do_field == 1){
                //Step through all the wavevectors
                for(k=1;k<=num_vectors;k++){
                    wv = 2*M_PI/sideLen * k;
                    Rk = 0;
                    for(sphere=0; sphere<spheres;sphere++){
                        Rk += cos(wv*X[sphere]);
                    }
                    
                    //Normalize and compute running stats
                    Rk /= vol_factor;
                    Rk_avg[k-1][0] += Rk;
                    Rk_avg[k-1][1] += Rk*Rk;
                    
                    //For select values of k take the histogram
                    if(k==Rk_hist_factor){
                        Rk_hist[sweep-equi_sweeps][0] = Rk;
                    }else if(k==2*Rk_hist_factor){
                        Rk_hist[sweep-equi_sweeps][1] = Rk;
                    }else if(k==3*Rk_hist_factor){
                        Rk_hist[sweep-equi_sweeps][2] = Rk;
                    }
                }    
            }
            
            //Calculate the radial distribution function
            if(do_radial == 1){                
                for(sphere=0; sphere < spheres; sphere++){
                    for(probe=sphere+1; probe < spheres; probe++){
                        
                        dX = X[sphere] - X[probe];
                        dX -= sideLen*round(dX/sideLen);
                        
                        dY = Y[sphere] - Y[probe];
                        dY -= sideLen*round(dY/sideLen);
                        
                        dZ = Z[sphere] - Z[probe];
                        dZ -= sideLen*round(dZ/sideLen);
                        
                        dR = sqrt(dX*dX + dY*dY + dZ*dZ);
                        
                        gr_bin= floor(dR/radial_hist_step);
                        gr_hist[gr_bin]++;
                    }
                }
               
            }
        }
        
        //Give a periodic status update
        if(sweep % percent == 0){
            printf("%d%%\n",sweep/percent);
        }
    }
    
    //Write the files
    printf("Writing the files...");
    
    if(do_track==1){
        FILE* traj= fopen(trajectory_file_name,"w");
        fprintf(traj,"#%d spheres, %d sweeps (post-equilibration), tracking %d\n",spheres,sweeps-equi_sweeps,tracker);
        fprintf(traj,"#SWEEP X Y Z");
        for(sweep=0; sweep < sweeps-equi_sweeps; sweep++){
            fprintf(traj,"%d %f %f %f\n",sweep,trajX[sweep],trajY[sweep],trajZ[sweep]);
        }
        fclose(traj);
    }
    
    if(do_probe==1){
        FILE* probe= fopen(probe_file_name,"w");
        fprintf(probe,"#%d spheres, %d sweeps (post-equlibration), %d probes\n",spheres,sweeps-equi_sweeps,probes);
        fprintf(probe,"#SPHERE 2D 3D 4D");
        for(sphere=0; sphere < spheres; sphere++){
            fprintf(probe,"%d %d %d %d\n",sphere,hist2d[sphere],hist3d[sphere],hist4d[sphere]);
        }
        fclose(probe);
    }
    
    if(do_field==1){
        FILE* field1= fopen(field_avg_file_name,"w");
        fprintf(field1,"#%d spheres, %d sweeps (post-equlibration), %d vectors\n",spheres,sweeps-equi_sweeps,num_vectors);
        for(k=0;k<num_vectors;k++){
            fprintf(field1,"%f %f\n",Rk_avg[k][0]/(sweeps-equi_sweeps),Rk_avg[k][1]/(sweeps-equi_sweeps));     
        }
        fclose(field1);
        
        FILE* field2= fopen(field_hist_file_name,"w");
        fprintf(field2,"#%d spheres, %d sweeps (post-equlibration), %d vectors\n",spheres,sweeps-equi_sweeps,num_vectors);
        for(sweep=0;sweep<sweeps-equi_sweeps;sweep++){
            fprintf(field2,"%f %f %f\n",Rk_hist[sweep][0],Rk_hist[sweep][1],Rk_hist[sweep][2]);     
        }
        fclose(field2);
    }
    
    
    if(do_radial == 1){
        FILE* radial= fopen(radial_hist_file_name,"w");
        //fprintf(radial,"#%d spheres, %d sweeps (post-equlibration)\n",spheres,sweeps-equi_sweeps);
        //fprintf(radial,"#SPHERE 2D 3D 4D");
        for(sweep=0; sweep<num_radial_bins; sweep++){
            fprintf(radial,"%f %d\n",sweep*radial_hist_step,gr_hist[sweep]);
        }
        fclose(radial);
    }
    
    printf("done!\n");
    
    //Count how much time elapsed
    end = clock();
    time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    printf("Finished in %4.2f seconds\n",time_spent);
}