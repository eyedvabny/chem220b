dens = [0.02 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9]; 
y = zeros(300,10);
for i = 1:10
   disp(sprintf('Calculating \\rho=%f',dens(i)));
   for j=1:300
        y(j,i) = ps4_q2_h((j-1)/30,dens(i));
        disp(sprintf('\t%d of 300',j));
   end
end