t = 0:0.01:10;

avg1 = mean(chi312);
avg2 = mean(chi324);
avg3 = mean(chi336);

figure(1);
plot(t,avg1,t,avg2,t,avg3);

xlabel('t/$\tau$');
ylabel('$\bar{X}(t)/\beta f_0$');
title('Collective variable behavior for $f_0=3$');
legend('$k=12\pi/L$','$k=24\pi/L$','$k=36\pi/L$');

avg1 = mean(chi512);
avg2 = mean(chi524);
avg3 = mean(chi536);

figure(2);
plot(t,avg1,t,avg2,t,avg3);

xlabel('t/$\tau$');
ylabel('$\bar{X}(t)/\beta f_0$');
title('Collective variable behavior for $f_0=5$');
legend('$k=12\pi/L$','$k=24\pi/L$','$k=36\pi/L$');


avg1 = mean(chi1012);
avg2 = mean(chi1024);
avg3 = mean(chi1036);

figure(3);
plot(t,avg1,t,avg2,t,avg3);

xlabel('t/$\tau$');
ylabel('$\bar{X}(t)/\beta f_0$');
title('Collective variable behavior for $f_0=10$');
legend('$k=12\pi/L$','$k=24\pi/L$','$k=36\pi/L$');

