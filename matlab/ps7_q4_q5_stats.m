drSq3 = [msd(p30); msd(p31); msd(p32); msd(p33); msd(p34); msd(p35); msd(p36); msd(p37); msd(p38); msd(p39)];
dr3 = [mean(drSq3);mean(drSq3)+std(drSq3);mean(drSq3)-std(drSq3)];

drSq6 = [msd(p60); msd(p61); msd(p62); msd(p63); msd(p64); msd(p65); msd(p66); msd(p67); msd(p68); msd(p69)];
dr6 = [mean(drSq6);mean(drSq6)+std(drSq6);mean(drSq6)-std(drSq6)];

drSq8 = [msd(p80); msd(p81); msd(p82); msd(p83); msd(p84); msd(p85); msd(p86); msd(p87); msd(p88); msd(p89)];
dr8 = [mean(drSq8);mean(drSq8)+std(drSq8);mean(drSq8)-std(drSq8)];

cvvV3 = [cvv(v30); cvv(v31); cvv(v32); cvv(v33); cvv(v34); cvv(v35); cvv(v36); cvv(v37); cvv(v38); cvv(v39)];
cvv3 = [mean(cvvV3);mean(cvvV3)+std(cvvV3);mean(cvvV3)-std(cvvV3)];

cvvV6 = [cvv(v60); cvv(v61); cvv(v62); cvv(v63); cvv(v64); cvv(v65); cvv(v66); cvv(v67); cvv(v68); cvv(v69)];
cvv6 = [mean(cvvV6);mean(cvvV6)+std(cvvV6);mean(cvvV6)-std(cvvV6)];

cvvV8 = [cvv(v80); cvv(v81); cvv(v82); cvv(v83); cvv(v84); cvv(v85); cvv(v86); cvv(v87); cvv(v88); cvv(v89)];
cvv8 = [mean(cvvV8);mean(cvvV8)+std(cvvV8);mean(cvvV8)-std(cvvV8)];
