figure();
plot(r,gr3,'b',r,gr6,'g',r,gr8,'r');
xlabel('Inter-particle distance r($$\sigma/$$)','Interpreter','latex','Fontsize',14);
ylabel('g(r)','Interpreter','latex','Fontsize',14);
title('Lennard-Jones Verlet MD: 343 particles, $$T^*=1.5$$','Interpreter','latex','Fontsize',14);
l = legend('$$\rho\sigma^3=0.3$$','$$\rho\sigma^3=0.6$$','$$\rho\sigma^3=0.8$$','Location','East');
set(l,'Interpreter','latex');