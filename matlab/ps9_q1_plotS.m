avg1= getS(s312);
avg2= getS(s324);
avg3= getS(s336);

figure(1);
plot(t,avg1,t,avg2,t,avg3);

xlabel('t/$\tau$');
ylabel('$S(k,t)$');
title('Intermediate Scattering Function for $f_0=3$');
legend('$k=12\pi/L$','$k=24\pi/L$','$k=36\pi/L$');

avg1= getS(s512);
avg2= getS(s524);
avg3= getS(s536);

figure(2);
plot(t,avg1,t,avg2,t,avg3);

xlabel('t/$\tau$');
ylabel('$S(k,t)$');
title('Intermediate Scattering Function for $f_0=5$');
legend('$k=12\pi/L$','$k=24\pi/L$','$k=36\pi/L$');

avg1= getS(s1012);
avg2= getS(s1024);
avg3= getS(s1036);

figure(3);
plot(t,avg1,t,avg2,t,avg3);

xlabel('t/$\tau$');
ylabel('$S(k,t)$');
title('Intermediate Scattering Function for $f_0=10$');
legend('$k=12\pi/L$','$k=24\pi/L$','$k=36\pi/L$');