% rA = solute size (scalar)
% k = 1st vector from NL file (change first value from 0 to 0.001)
% rhww = 2nd vector from NL file 


function sols = compute_coefs( rA,k,rhww )

    %PART 1: Find the coefficients of Caw
    syms c0 c1 c2 c3 real;
    coefs = [c0 c1 c2 c3];
    
    rW = 2.7; %Radius of water
    R = (rW + rA)/2; %Radius of solute
    
    % basis function
    phi = @(r,n) ((r-R)/R).^n .* heaviside(R-r);
    
    % overlaps
    overlap = @(r) phi(r,0:3);
    
    % fourier transforms of overlaps
    ftphi0 = @(k) ( sin(k*R)./k.^3 - R*cos(k*R)./k.^2);
    ftphi1 = @(k) ( sin(k*R)./k.^3 + 2*(cos(k*R)-1)./(R*k.^4));
    ftphi2 = @(k) ( -6*sin(k*R)./(R^2*k.^5) + 2*(cos(k*R)+2)./(R*k.^4));
    ftphi3 = @(k) ( -6*sin(k*R)./(R^2*k.^5) + 24*(1-cos(k*R))./(R^3*k.^6) - 6./(R*k.^4));
    
    % make it so I can get one of the ft functions just by passing in n
    ftphi = {ftphi0;ftphi1;ftphi2;ftphi3};
    
    % perform an integral using riemann sums
    integrand = @(n,m,k) k.^2 .* ftphi{n+1}(k) .* ftphi{m+1}(k) .* rhww;
    intg = @(n,m) 8 * trapz(k,integrand(n,m,k));
    
    % equation components
    p1 = @(n) integral(@(r)4*pi*r.^2.*phi(r,n),0,R);
    p2 = @(n) dot(coefs,integral(@(r)4*pi*r.^2.*phi(r,n)*overlap(r),0,R,'ArrayValued',true));
    p3 = @(n) c0*intg(n,0) + c1*intg(n,1) + c2*intg(n,2) + c3*intg(n,3);
    eq = @(n) p1(n) + p2(n) + p3(n);
   
     % solve the system for the coefficients
    [a1 a2 a3 a4] = solve(eq(0)==0,eq(1)==0,eq(2)==0,eq(3)==0);
    sols = [double(a1) double(a2) double(a3) double(a4)];
end

