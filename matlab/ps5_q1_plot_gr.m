r = linspace(-1,10,500);

gr3 = arrayfun(@(r)ps4_q1_gr(r,3,coefs(4,:),k,rhww),r);
gr4 = arrayfun(@(r)ps4_q1_gr(r,4,coefs(14,:),k,rhww),r);
gr5 = arrayfun(@(r)ps4_q1_gr(r,5,coefs(25,:),k,rhww),r);

% Plot the combined plot
figure();
hold on;
plot(r,gr3,'r');
plot(r,gr4,'g');
plot(r,gr5,'b');

xlabel('r(A)');
ylabel('g_{AW}(r)');
title('Radial distribution g_{AW}');
legend('3','4','5');