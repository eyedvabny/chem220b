function out = getD( t,stats )
    out = zeros(1,2);
    dim = size(stats,1);
    vals = zeros(1,dim);
    for i=1:dim
        p = polyfit(t,stats(i,:),1);
        vals(i) = p(1);
    end
    out(1) = mean(vals)/6;
    out(2) = std(vals)/6;
end

