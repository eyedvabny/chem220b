figure();
plot(t,dr3(1,:));
hold on;
plot(t(1:20:end),dr3(2,1:20:end),'ro');
plot(t(1:20:end),dr3(3,1:20:end),'go');


title('Lennard-Jones Verlet MD: 343 particles, $$T^*=1.5, \rho\sigma^3=0.3$$','Interpreter','latex','Fontsize',14);
xlabel('Time($$\tau$$)','Interpreter','latex','Fontsize',14);
ylabel('$$\langle \Delta \bar{r}^2(t/\tau)\rangle$$','Interpreter','latex','Fontsize',14);
l = legend('$$\bar{x}$$','$$\bar{x}+\sigma$$','$$\bar{x}-\sigma$$','Location','West');
set(l,'Interpreter','latex');

figure();
plot(t,dr6(1,:));
hold on;
plot(t(1:20:end),dr6(2,1:20:end),'ro');
plot(t(1:20:end),dr6(3,1:20:end),'go');


title('Lennard-Jones Verlet MD: 343 particles, $$T^*=1.5, \rho\sigma^3=0.6$$','Interpreter','latex','Fontsize',14);
xlabel('Time($$\tau$$)','Interpreter','latex','Fontsize',14);
ylabel('$$\langle \Delta \bar{r}^2(t/\tau)\rangle$$','Interpreter','latex','Fontsize',14);
l = legend('$$\bar{x}$$','$$\bar{x}+\sigma$$','$$\bar{x}-\sigma$$','Location','West');
set(l,'Interpreter','latex');

figure();
plot(t,dr8(1,:));
hold on;
plot(t(1:20:end),dr8(2,1:20:end),'ro');
plot(t(1:20:end),dr8(3,1:20:end),'go');


title('Lennard-Jones Verlet MD: 343 particles, $$T^*=1.5, \rho\sigma^3=0.8$$','Interpreter','latex','Fontsize',14);
xlabel('Time($$\tau$$)','Interpreter','latex','Fontsize',14);
ylabel('$$\langle \Delta \bar{r}^2(t/\tau)\rangle$$','Interpreter','latex','Fontsize',14);
l = legend('$$\bar{x}$$','$$\bar{x}+\sigma$$','$$\bar{x}-\sigma$$','Location','West');
set(l,'Interpreter','latex');