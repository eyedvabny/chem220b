figure();
plot(ve,pv,'b',ve,pve03,'go',ve,pve08,'rs');
xlabel('Velocity ($$\sigma/\tau$$)','Interpreter','latex','Fontsize',14);
ylabel('Probability','Interpreter','latex','Fontsize',14);
title('Lennard-Jones Verlet MD: 1000 particles, $$T^*=1.5$$','Interpreter','latex','Fontsize',14);
l = legend('$$P_{MB}(v)$$','$$\rho\sigma^3=0.3$$','$$\rho\sigma^3=0.8$$','Location','East');
set(l,'Interpreter','latex');