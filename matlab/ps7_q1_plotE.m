figure();
plot(t,U,t,K,t,E);
xlabel('Time ($$\tau$$)','Interpreter','latex','Fontsize',14);
ylabel('Energy per particle ($$\epsilon$$)','Interpreter','latex','Fontsize',14);
title('Lennard-Jones Verlet MD: 1000 particles, $$\rho\sigma^3=0.8,T^*=1.5$$','Interpreter','latex','Fontsize',14);
legend('Potential','Kinetic','Total','Location','East');