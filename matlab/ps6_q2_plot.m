
%Plot r vs t
figure();
plot(t,r);
xlabel('t');
ylabel('r(t))');
title('Position of Harmonic Oscillator');
axis([0 63 -1 1]);

%Plot v vs t
figure();
plot(t,v);
xlabel('t');
ylabel('v(t)');
title('Velocity of Harmonic Oscillator');
axis([0 63 -1 1]);

%Calculate the energies
U=0.5*r.^2;
KE=0.5*v.^2;
E=U+KE;

%Plot U vs t
figure();
plot(t,U);
xlabel('t');
ylabel('U(t)');
title('Potential Energy of Harmonic Oscillator');
axis([0 63 0 0.5]);

%Plot KE vs t
figure();
plot(t,KE);
xlabel('t');
ylabel('KE(t)');
title('Kinetic Energy of Harmonic Oscillator');
axis([0 63 0 0.5]);

%Plot E vs t
figure();
plot(t,E);
xlabel('t');
ylabel('E(t)');
title('Total Energy of Harmonic Oscillator');
axis([0 63 0 1]);

figure();
plot(r,v);
xlabel('r(t)');
ylabel('v(t)');
title('Phase Portrait of Harmonic Oscillator');
axis square;

[p,x] = hist(r,10);
p = p/sum(p);
figure()
plot(x,p);
xlabel('r');
ylabel('P(r)');
title('Probability distribution of Harmonic Oscillator');


