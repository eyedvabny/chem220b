for i=1:10   
   figure();
   plot(x,y(:,i));
   
   ttl = sprintf('h(r) for \\rho =%.2f',dens(i));
   title(ttl);
   ylabel('h(r)');
   xlabel('r');
end