function r2 = msd (p)
    r2 = zeros(1,1001);
    for dt=0:1000
        for step=1:length(p)-dt
            dx = p(step+dt,1)-p(step,1);
            dy = p(step+dt,2)-p(step,2);
            dz = p(step+dt,3)-p(step,3);
            
            dr2 = dx*dx + dy*dy + dz*dz;
            
            r2(dt+1) = r2(dt+1) + dr2;
        end
        r2(dt+1) = r2(dt+1)/(length(p)-dt);
    end 
end