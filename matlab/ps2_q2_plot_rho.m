function plotDensityField(Rk,Rk2,k1,k2,k3)

%Plot Rk to show it's centered around 0
figure(1);
plot(Rk,'s','MarkerFaceColor','b');
hold on;
plot(zeros(size(Rk)),'--r');
hold off;
xlabel('k (2\pi/L)');
ylabel('<R_k>');
title('Fluctuation of FT density field of HS model');
axis([0 length(Rk) -max(Rk) max(Rk)]);

%Plot Rk2 and highlight the mean
figure(2);
plot(4*Rk2);
hold on;
plot(ones(size(Rk2)),'--r');
hold off;
xlabel('k (2\pi/L)');
ylabel('S(k)');
title('Static structure factor of HS model');
axis([0 length(Rk2) 0 4*max(Rk2)]);

%Plot k=pi/d
figure(3);
[h, xout] = hist(k1,100);
plot(xout,log(h/trapz(xout,h)),'ob');

mu = mean(k1);
sigma = std(k1);

hold on;
plot(xout,log(normpdf(xout,mu,sigma)),'r');
hold off;

xlabel('R_k');
ylabel('ln[P(R_k)]');
title('Density field distribution for k=\pi/d');
axis([-1 1 -6 1]);

%Plot k=2*pi/d
figure(4);
[h, xout] = hist(k2,100);
plot(xout,log(h/trapz(xout,h)),'ob');

mu = mean(k2);
sigma = std(k2);

hold on;
plot(xout,log(normpdf(xout,mu,sigma)),'r');
hold off;

xlabel('R_k');
ylabel('ln[P(R_k)]');
title('Density field distribution for k=\pi2/d');
axis([-2.5 2.5 -7 0]);

%Plot k=3*pi/d
figure(5);
[h, xout] = hist(k3,100);
plot(xout,log(h/trapz(xout,h)),'ob');

mu = mean(k3);
sigma = std(k3);

hold on;
plot(xout,log(normpdf(xout,mu,sigma)),'r');
hold off;

xlabel('R_k');
ylabel('ln[P(R_k)]');
title('Density field distribution for k=\pi3/d');
axis([-2 2 -7 0]);
end

