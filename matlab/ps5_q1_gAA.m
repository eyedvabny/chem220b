function gAA = compute_gAA(r,rA,coefs,k,rhww)

    rW = 2.7; %Radius of water
    R = (rW + rA)/2; %Radius of solute
    rhoW = 0.033; % density
    
    % basis function
    phi = @(r,n) ((r-R)/R).^n .* heaviside(R-r);
    
    % fourier transforms of overlaps
    ftphi0 = @(k) 4*pi*( sin(k*R)./k.^3 - R*cos(k*R)./k.^2);
    ftphi1 = @(k) 4*pi*( sin(k*R)./k.^3 + 2*(cos(k*R)-1)./(R*k.^4));
    ftphi2 = @(k) 4*pi*( -6*sin(k*R)./(R^2*k.^5) + 2*(cos(k*R)+2)./(R*k.^4));
    ftphi3 = @(k) 4*pi*( -6*sin(k*R)./(R^2*k.^5) + 24*(1-cos(k*R))./(R^3*k.^6) - 6./(R*k.^4));
    
    % define Caw and FT of Caw
    Caw = @(r) coefs(1)*phi(r,0) + coefs(2)*phi(r,1) + coefs(3)*phi(r,2) + coefs(4)*phi(r,3);
    ftCaw = @(k) coefs(1)*ftphi0(k) + coefs(2)*ftphi1(k) + coefs(3)*ftphi2(k) + coefs(4)*ftphi3(k);
    
    ftHaa = @(k) rhoW .* ftCaw(k).^2 .* (1 + rhww);
    
    integrand = @(k,r) k .* sin(k*r) .* ftHaa(k);
    intg = @(r) 1/(2*pi^2*r) * trapz(k,integrand(k,r));
    
    gAA = (arrayfun(intg,r) + 1) * heaviside(r-rA);
end