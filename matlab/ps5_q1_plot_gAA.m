gAA3 = arrayfun(@(r) ps4_q1_gAA(r,3,coefs(4,:),k,rhww),r);
gAA4 = arrayfun(@(r) ps4_q1_gAA(r,4,coefs(14,:),k,rhww),r);
gAA5 = arrayfun(@(r) ps4_q1_gAA(r,5,coefs(25,:),k,rhww),r);

figure();
plot(r,gAA3);
xlabel('r(A)');
ylabel('g_{AA}(r)');
title('Radial distribution function of solute for \sigma_A=3');

figure();
plot(r,gAA4);
xlabel('r(A)');
ylabel('g_{AA}(r)');
title('Radial distribution function of solute for \sigma_A=4');

figure();
plot(r,gAA5);
xlabel('r(A)');
ylabel('g_{AA}(r)');
title('Radial distribution function of solute for \sigma_A=5');