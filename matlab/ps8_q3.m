figure(1);
plot(ts,p1,ts,pl1,ts,ph1);
xlabel('$|t|/\tau$');
ylabel('$\phi(t)$');
legend('Actual','Low Limit','High Limit');
title('$\phi(t)$ for $\alpha=0.1$');

figure(2);
plot(tl,p2,tl,pl2,tl,ph2);
xlabel('$|t|/\tau$');
ylabel('$\phi(t)$');
legend('Actual','Low Limit','High Limit');
title('$\phi(t)$ for $\alpha=0.1$');

figure(3);
plot(ts,p3,ts,pl3);
xlabel('$|t|/\tau$');
ylabel('$\phi(t)$');
legend('Actual','Low Limit');
title('$\phi(t)$ for $\alpha=10$');

figure(4);
plot(tl,p4,tl,pl4);
xlabel('$|t|/\tau$');
ylabel('$\phi(t)$');
legend('Actual','Low Limit');
title('$\phi(t)$ for $\alpha=10$');