figure();
plot(t,cvv3(1,:));
hold on;
plot(t(1:20:end),cvv3(2,1:20:end),'ro');
plot(t(1:20:end),cvv3(3,1:20:end),'go');


title('Lennard-Jones Verlet MD: 343 particles, $$T^*=1.5, \rho\sigma^3=0.3$$','Interpreter','latex','Fontsize',14);
xlabel('Time($$\tau$$)','Interpreter','latex','Fontsize',14);
ylabel('$$C_{vv}(t)$$','Interpreter','latex','Fontsize',14);
l = legend('$$\bar{x}$$','$$\bar{x}+\sigma$$','$$\bar{x}-\sigma$$','Location','East');
set(l,'Interpreter','latex');

figure();
plot(t,cvv6(1,:));
hold on;
plot(t(1:20:end),cvv6(2,1:20:end),'ro');
plot(t(1:20:end),cvv6(3,1:20:end),'go');


title('Lennard-Jones Verlet MD: 343 particles, $$T^*=1.5, \rho\sigma^3=0.6$$','Interpreter','latex','Fontsize',14);
xlabel('Time($$\tau$$)','Interpreter','latex','Fontsize',14);
ylabel('$$C_{vv}(t)$$','Interpreter','latex','Fontsize',14);
l = legend('$$\bar{x}$$','$$\bar{x}+\sigma$$','$$\bar{x}-\sigma$$','Location','East');
set(l,'Interpreter','latex');

figure();
plot(t,cvv8(1,:));
hold on;
plot(t(1:20:end),cvv8(2,1:20:end),'ro');
plot(t(1:20:end),cvv8(3,1:20:end),'go');


title('Lennard-Jones Verlet MD: 343 particles, $$T^*=1.5, \rho\sigma^3=0.8$$','Interpreter','latex','Fontsize',14);
xlabel('Time($$\tau$$)','Interpreter','latex','Fontsize',14);
ylabel('$$C_{vv}(t)$$','Interpreter','latex','Fontsize',14);
l = legend('$$\bar{x}$$','$$\bar{x}+\sigma$$','$$\bar{x}-\sigma$$','Location','East');
set(l,'Interpreter','latex');
