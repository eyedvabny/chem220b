function h = h(r,rho)

n = pi/6*rho;

lamda1 = -(1+2*n)^2/(1-n)^4;
lamda2 = 6*n*(1+n/2)^2/(1-n)^4;
lamda3 = n*lamda1/2;

int1 = @(k) 4*pi*(-cos(k)./k.^2 + sin(k)./k.^3);
int2 = @(k) 4*pi*(-cos(k)./k.^2 + 2*sin(k)./k.^3 + 2*cos(k)./k.^4 - 2./k.^4);
int3 = @(k) 4*pi*(-cos(k)./k.^2 + 4*sin(k)./k.^3 + 12*cos(k)./k.^4 - 24*sin(k)./k.^5 - 24*cos(k)./k.^6 + 24./k.^6);

ck = @(k) lamda1*int1(k) + lamda2*int2(k) + lamda3*int3(k);

Ik = @(k) (rho*ck(k).^2)./(1-rho*ck(k));

integrand = @(k) Ik(k).*sin(k*r).*k;

Ir = 1./(2*pi^2*r) * integral(integrand,0.001,Inf);

cr = (lamda1+lamda2*r+lamda3*r.^3)*heaviside(1-r);

h = Ir + cr;

end

