function vaf = cvv (v)
    vaf = zeros(1,1001);
    for dt=0:1000
        for step=1:length(v)-dt
            dvx = v(step+dt,1)*v(step,1);
            dvy = v(step+dt,2)*v(step,2);
            dvz = v(step+dt,3)*v(step,3);
            
            dv = dvx + dvy + dvz;
            
            vaf(dt+1) = vaf(1,dt+1) + dv;
        end
        vaf(dt+1) = vaf(dt+1)/(length(v)-dt);
    end 
end