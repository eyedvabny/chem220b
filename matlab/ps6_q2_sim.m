function [r,v,t] = verlet(tot_time,dt)
    num = floor(tot_time/dt);
    t = 0:dt:tot_time;
    r= ones(1,num+1);
    v= zeros(1,num+1);
    for i=2:num
        r(i)=r(i-1)+dt*v(i-1)-0.5*dt^2*r(i-1);
        v(i)=v(i-1)-0.5*dt*(r(i-1)+r(i));
    end
end

