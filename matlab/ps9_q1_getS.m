function S = getS (data)
    S = zeros(1,1001);
    for dt=0:1000
        for step=1:length(data)-dt
            dS = data(step+dt,1)*data(step,1);
            S(dt+1) = S(dt+1) + dS;
        end
        S(dt+1) = S(dt+1)/(length(data)-dt)/125;
    end 
end