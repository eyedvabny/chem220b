rW = 2.7; %Radius of water
rhoW = 0.033; % density
kBT = (1.38065*10^-23)*298;

R = linspace(2.025,5.025,61);
expot = zeros(1,61);
values = zeros(1,61);

for i=1:61
   values(i) = R(i).^2 .* ps4_q1_gr(0.001,rA(i),coefs(i,:),k,rhww);
end


for i=2:61
    x = R(1:i);
    y = values(1:i);
    expot(i) = 4*pi*rhoW*kBT* trapz(x,y);
end
   
figure();
plot(R,expot,'-O');
xlabel('R(A)');
ylabel('\Delta\mu_{ex}(R) in J');
title('Change in excess potential');
axis([2.6 5.1 0 1]);
axis 'auto y';