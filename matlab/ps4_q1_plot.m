function out = plot_gr(x,y,n,d)
mod =(n-1)/(4*pi*d);
y=y/trapz(x,y);
y=y./x.^2;
out=y*mod;

%figure();
%lot(x,y)

%ttl = sprintf('Radial distribution function\n%d spheres,\\rho=%.2f',n,d);
%title(ttl);
%ylabel('g(r)');
%xlabel('r');
end

