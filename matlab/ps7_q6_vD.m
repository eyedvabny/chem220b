function out = getD( t,stats )
    out = zeros(1,2);
    dim = size(stats,1);
    vals = zeros(1,dim);
    for i=1:dim
        vals(i) = trapz(t,stats(i,:));
    end
    out(1) = mean(vals)/3;
    out(2) = std(vals)/3;
end