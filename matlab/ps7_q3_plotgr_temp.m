figure();
plot(r,gr8,'b',r,gr82,'r');
xlabel('Inter-particle distance r($$\sigma$$)','Interpreter','latex','Fontsize',14);
ylabel('g(r)','Interpreter','latex','Fontsize',14);
title('Lennard-Jones Verlet MD: 343 particles, $$\rho\sigma^3=0.8$$','Interpreter','latex','Fontsize',14);
l = legend('$$T^*=1.5$$','$$T^*=3.5$$','Location','East');
set(l,'Interpreter','latex');