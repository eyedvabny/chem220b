figure();
plot(x,y1,'b');
hold on;
plot(x,y(:,2)+1,'r');
hold off;

title('Radial distribution function for \rho=0.1');
ylabel('g(r)');
xlabel('r');

figure();
plot(x,y4,'b');
hold on;
plot(x,y(:,5)+1,'r');
hold off;

title('Radial distribution function for \rho=0.4');
ylabel('g(r)');
xlabel('r');

figure();
plot(x,y7,'b');
hold on;
plot(x,y(:,8)+1,'r');
hold off;

title('Radial distribution function for \rho=0.7');
ylabel('g(r)');
xlabel('r');

figure();
plot(x,y9,'b');
hold on;
plot(x,y(:,10)+1,'r');
hold off;

title('Radial distribution function for \rho=0.9');
ylabel('g(r)');
xlabel('r');