//
//  verlet.cpp
//  verlet
//
//  Created by Eugene Yedvabny on 3/28/13.
//  Copyright (c) 2013 Eugene Yedvabny. All rights reserved.
//

#include <iostream>
#include <ctime>
#include <cmath>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_real_distribution.hpp>
#include "verlet.h"

//Init the random generator
boost::random::mt19937 rng(std::time(0));

/***** VERLET METHODS *****/

//Specify number of particles and desired density
Verlet::Verlet(int np, double d, double c, double t):
    sigma(1.0),
    num_particles(np),
    density(d),
    cutoff(c),
    temp(t),
    particles(np)
    {}

//Allocate the particles and position them across the lattice
//Randomly allocate the initial velocities
void Verlet::initialize(){

    //Compute the energy scaling factor
    //energy_factor = 4*(pow(cutoff,-12)-pow(cutoff,-6));
    
    //Calculate the side length of the box
    side_length = cbrt(num_particles*sigma/density);
    int num_per_side = cbrt(num_particles);
    double spacing = side_length / num_per_side;
    
    //Create a uniform generator to give velocities
    //Limited by the specified reduced temperature
    double v_limit = sqrt(temp/16.0);
    boost::random::uniform_real_distribution<double> rand_v(-v_limit,v_limit);

    //Reset the chi
    S = 0.0;

    //Place the particles on a simple lattice
    //Randomize the velocities
    double x,y,z,vx,vy,vz;
    for(int particle=0; particle<num_particles; particle++){
        
        //Simple cubic lattice
        x =(particle % num_per_side) * spacing;
        y =((particle / num_per_side) % num_per_side) * spacing;
        z =(particle / num_per_side / num_per_side) * spacing;
        particles[particle].setR(x,y,z);
        
        //Random velocities on +-v_limit
        vx = rand_v(rng);
        vy = rand_v(rng);
        vz = rand_v(rng);
        particles[particle].setV(vx,vy,vz);

        //Reset the acceleration
        particles[particle].zeroA();
    }

    //Compute the first round of accelerations and U
    //Go through all the particle pairs
    for (int i=0; i<num_particles-1; i++){
        for(int j=i+1; j<num_particles; j++){
            Verlet::applyForce(particles[i],particles[j],cutoff);
        }
    }
}

//Equilibrate the velocities
void Verlet::equilibrate(){
    double vel[3]={0}, tot_vel[3]={0}, tot_v2=0;

    //Get the current velocity info
    for(int i=0; i<num_particles; i++){
        particles[i].getV(vel);

        //Keep track of total velocities
        tot_vel[0]+=vel[0];
        tot_vel[1]+=vel[1];
        tot_vel[2]+=vel[2];
        tot_v2+=vel[0]*vel[0] + vel[1]*vel[1] + vel[2]*vel[2];
    }

    //Calculate the scaling factor
    double v_scale = sqrt(3*temp*(num_particles-1)/48/tot_v2);

    //Rescale the velocities
    double vx,vy,vz;
    for(int i=0; i<num_particles; i++){
        particles[i].getV(vel);

        vx=(vel[0]-tot_vel[0]/num_particles)*v_scale;
        vy=(vel[1]-tot_vel[1]/num_particles)*v_scale;
        vz=(vel[2]-tot_vel[2]/num_particles)*v_scale;

        particles[i].setV(vx, vy, vz);
    }
}

//Propagate the system by one step
//Assume the current accelerations
//have already been computed
void Verlet::propagate(double dt, double pf, double wv){

    //Containers
    double pos[3];
    double vel[3];
    double acc[3];
    double oacc[3];

    //Reset chi
    S = 0.0;

    //Update the positions
    double x,y,z;
    for(int i=0; i<num_particles; i++){
        particles[i].getR(pos);
        particles[i].getV(vel);
        particles[i].getA(acc);

        x = pos[0]+dt*vel[0]+0.5*dt*dt*acc[0];
        y = pos[1]+dt*vel[1]+0.5*dt*dt*acc[1];
        z = pos[2]+dt*vel[2]+0.5*dt*dt*acc[2];

        particles[i].setR(x,y,z);

        //Calculate the chi based on the current position
        S += cos(wv*x);

        //Store away the current accelerations
        //Zeros the previous accelerations too
        particles[i].copyA();
    }

    //Calculate the acceleration again with new positions
    //Will also calculate U for the next step
    for (int i=0; i<num_particles-1; i++){
        for(int j=i+1; j<num_particles; j++){
            Verlet::applyForce(particles[i],particles[j],cutoff);
        }
    }

    //Add the accelerations due to perturbation
    if(pf != 0.0){
        double pert_acc;
        for (int i=0; i<num_particles; i++){
            particles[i].getR(pos);
            particles[i].getA(acc);

            //Divided by 48 to preserve the non-dimentionality
            pert_acc = pf*wv*sin(wv*pos[0])/sqrt(num_particles)/48;

            particles[i].setA(acc[0]-pert_acc,acc[1],acc[2]);
        }
    }

    //Update the velocities
    double vx,vy,vz;
    for(int i=0; i<num_particles; i++){
        
        particles[i].getV(vel);
        particles[i].getA(acc);
        particles[i].getOldA(oacc);

        vx = vel[0]+0.5*dt*(oacc[0] + acc[0]);
        vy = vel[1]+0.5*dt*(oacc[1] + acc[1]);
        vz = vel[2]+0.5*dt*(oacc[2] + acc[2]);

        particles[i].setV(vx,vy,vz);
    }
}

//Compute and apply the LJ force between two particles
void Verlet::applyForce(Particle &p1, Particle &p2, double cutoff){
    
    double pos_p1[3], pos_p2[3], acc_p1[3], acc_p2[3];
    
    //Get the distance between two particles
    p1.getR(pos_p1);
    p2.getR(pos_p2);

    double dx = pos_p1[0]-pos_p2[0];
    dx -= side_length*round(dx/side_length);
    double dy = pos_p1[1]-pos_p2[1];
    dy -= side_length*round(dy/side_length);
    double dz = pos_p1[2]-pos_p2[2];
    dz -= side_length*round(dz/side_length);

    double dr2 = dx*dx + dy*dy + dz*dz;
    
    //Only calculate forces if separation < r*
    if(dr2 > cutoff*cutoff) return;

    //Calculate the Lennard-Jones forces
    //F = 48(r^-14 - 0.5*r^-8) in each direction
    //But 48 cancels with reduced units
    double a = pow(dr2,-7) - 0.5*pow(dr2,-4);

    //Update the acceleration
    p1.getA(acc_p1);
    p2.getA(acc_p2);

    p1.setA(acc_p1[0]+a*dx,acc_p1[1]+a*dy,acc_p1[2]+a*dz);
    p2.setA(acc_p2[0]-a*dx,acc_p2[1]-a*dy,acc_p2[2]-a*dz);
}

double Verlet::getS(){
    return S;
}

Verlet::~Verlet(){
    particles.clear();
}