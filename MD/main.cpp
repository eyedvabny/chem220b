//
//  main.cpp
//  verlet
//
//  Created by Eugene Yedvabny on 3/28/13.
//  Copyright (c) 2013 Eugene Yedvabny. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <ctime>
#include "verlet.h"

#define NUM_OPTIONS 6 //Number of options to require

#define TAG 100 //The particle to observe

#define CUTOFF 2.5
#define EQ_STEPS 300
#define TIME_STEP 0.01

int main(int argc, const char * argv[]){

    //Greetings
    std::cout << "Velocity Verlet Molecular Dynamics" << std::endl;
    
    //Containers for parameters
    int num_particles;
    double density;
    double cutoff;
    double temperature;
    double time_step;
    double tot_time;
    double eq_steps;
    double force_bias;
    double wv_coef;

    //For timing
    clock_t begin;
    
    //No arguments = print usage
    if(argc < NUM_OPTIONS){
        std::cerr << "Error: wrong arguments provided" << std::endl;
        std::cerr << "Usage: verlet num_particles density temperature tot_time ext_force wavevector_coef" << std::endl;
        return 0;
    }

    //Otherwise process the arguments into the parameters
    else{
        num_particles = atoi(argv[1]);
        density = atof(argv[2]);
        temperature = atof(argv[3]);
        tot_time = atof(argv[4]);
        force_bias = atof(argv[5]);
        wv_coef = atof(argv[6]);

        time_step = TIME_STEP;
        eq_steps = EQ_STEPS;
        cutoff = CUTOFF;
    }

    //Start the simulation
    begin = clock();
    std::cout << "Simulating" << std::endl;

    //Calculate the number of time steps
    int num_steps = ceil(tot_time/time_step);

    //Calculate the actual wavevector from the coefficient
    double side_length = cbrt(num_particles/density);
    double wv = wv_coef*M_PI/side_length;

    //Container for the data
    double S_data[num_steps+1];

    //The actual simulation class
    Verlet sim(num_particles,density,cutoff,temperature);

    /***** INITIALIZATION *****/
    sim.initialize();

    /***** EQUILIBRATION *****/

    for(int eq=1; eq<eq_steps; eq++){
        sim.equilibrate();
        sim.propagate(time_step, 0.0, wv);
    }

    /***** BIASED PROPAGATION *****/

    for(int step=0; step < num_steps; step++){
        sim.propagate(time_step, force_bias, wv);
    }

    /***** SIMULATION *****/
    S_data[0] = sim.getS();
    
    for(int step=1; step <= num_steps; step++){
        sim.propagate(time_step, 0.0, wv);

        //Calculate the current chi and print it to file
        S_data[step] = sim.getS();
    }

    std::cout << "Write out the data file" << std::endl;

    //Write out the data
    std::stringstream name;
    name << "S_" << force_bias << "_" << wv_coef << ".dat";
    std::ofstream S_file (name.str().c_str());

    for(int j=0; j<=num_steps; j++){
        S_file << S_data[j] << std::endl;
    }

    S_file.close();

    std::cout << "Done in " << (clock()-begin)/CLOCKS_PER_SEC << " seconds" << std::endl;

    //Good bye
    return 0;
}

