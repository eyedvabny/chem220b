//
//  particle.h
//  verlet
//
//  Created by Eugene Yedvabny on 3/28/13.
//  Copyright (c) 2013 Eugene Yedvabny. All rights reserved.
//

#ifndef __verlet__particle__
#define __verlet__particle__

class Particle {
    
private:

    //Position
    double _x, _y, _z;
    
    //Velocity
    double _vx, _vy, _vz;
    
    //Acceleration
    double _ax, _ay, _az;

    //Old acceleration
    double _oax, _oay, _oaz;
    
    //Particle index
    const int index;
    
    //Global index counter
    static int globalID;

public:
    
    //Default constructor
    Particle();
    
    //Position
    void setR(double x, double y, double z);
    void getR(double* arr);
    
    //Velocity
    void setV(double vx, double vy, double vz);
    void getV(double* arr);
    
    //Acceleration
    void setA(double ax, double ay, double az);
    void getA(double* arr);

    //Older accelerations
    void copyA();
    void getOldA(double* arr);
    
    //Resetting functions
    void zeroA();
    
    //Get the index
    int getIndex();
};

#endif /* defined(__verlet__particle__) */
