//
//  verlet.h
//  verlet
//
//  Created by Eugene Yedvabny on 3/28/13.
//  Copyright (c) 2013 Eugene Yedvabny. All rights reserved.
//

#ifndef __verlet__verlet__
#define __verlet__verlet__

#include <fstream>
#include <vector>
#include "particle.h"


class Verlet {

private:
    const double sigma; //Particle diameter
    const int num_particles; //Number of particles
    const double density; //Density of the system
    const double cutoff; //Limit to the force interactions
    const double temp; //Reduced-unit temperature

    double side_length; //Dimension of the box
    double S; //The collective variable
    std::vector<Particle> particles; //Array to hold the particles
    
    //Apply LJ forces on two particles
    void applyForce(Particle &p1, Particle &p2, double cutoff);
    
public:
    
    //Specify number of particles, density
    Verlet(int np, double d, double c, double t);
    
    //Destructor to clean up the particle array
    ~Verlet();
    
    //Place the particles on a grid and init the velocities
    void initialize();

    //Equilibrate the velocities
    void equilibrate();
    
    //Propagate the system by a single time step
    //Specify the time step to the function call
    void propagate(double dt, double pf, double wv);

    double getS();
};

#endif /* defined(__verlet__verlet__) */
