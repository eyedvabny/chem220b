//
//  particle.cpp
//  verlet
//
//  Created by Eugene Yedvabny on 3/28/13.
//  Copyright (c) 2013 Eugene Yedvabny. All rights reserved.
//

#include "particle.h"

//Initialize the global ID
int Particle::globalID = 0;

//Default Constructor
Particle::Particle():
    _x(0.0),_y(0.0),_z(0.0),
    _vx(0.0),_vy(0.0),_vz(0.0),
    _ax(0.0),_ay(0.0),_az(0.0),
    _oax(0.0),_oay(0.0),_oaz(0.0),
    index(++globalID){}

void Particle::setR(double x, double y, double z){
    _x = x;
    _y = y;
    _z = z;
}

void Particle::getR(double* arr){
    arr[0] = _x;
    arr[1] = _y;
    arr[2] = _z;
}
    
void Particle::setV(double vx, double vy, double vz){
    _vx = vx;
    _vy = vy;
    _vz = vz;
}

void Particle::getV(double* arr){
    arr[0] = _vx;
    arr[1] = _vy;
    arr[2] = _vz;
}
    
void Particle::setA(double ax, double ay, double az){
    _ax = ax;
    _ay = ay;
    _az = az;
}

void Particle::getA(double* arr){
    arr[0] = _ax;
    arr[1] = _ay;
    arr[2] = _az;
}

void Particle::copyA(){
    _oax = _ax;
    _ax = 0.0;
    _oay = _ay;
    _ay = 0.0;
    _oaz = _az;
    _az = 0.0;
}

void Particle::getOldA(double* arr){
    arr[0] = _oax;
    arr[1] = _oay;
    arr[2] = _oaz;
}

void Particle::zeroA(){
    _ax = 0.0;
    _ay = 0.0;
    _az = 0.0;
}

int Particle::getIndex(){
    return index;
}